#[macro_export]
macro_rules! define_scanner {
    ($name:ident, $inputs:expr, $outputs:expr) => {
        pub struct $name<I: InputPin, O: OutputPin> {
            inputs: [I; $inputs],
            outputs: [O; $outputs],
        }

        impl<I, O> $name<I, O>
            where I: InputPin,
                  O: OutputPin,
                  I::Error: core::fmt::Debug,
                  O::Error: core::fmt::Debug
        {
            pub fn new(inputs: [I; $inputs], outputs: [O; $outputs]) -> Self {
                Self {
                    inputs, outputs,
                }
            }

            pub fn set_leds(&mut self){
                let mut index = 0;
                for output in &mut self.outputs {
                    if self.inputs[index].is_low().unwrap() {
                        output.set_high().unwrap();
                    }
                    else{
                        output.set_low().unwrap();
                    }
                    index+=1;
                }
            }
        }
    }
}
