#![no_main]
#![no_std]

use panic_semihosting as _;
use rtfm::app;

use cortex_m::asm;

use embedded_hal::digital::v2::{InputPin, OutputPin};
use stm32f1xx_hal::{
    prelude::*,
    pac,
    delay::Delay,
    usb,
    gpio::{Output, PushPull, Input, Pxx, PullDown},
    timer::{Timer, CountDownTimer}
};
use usb_device::prelude::*;
use usb_device::bus::UsbBusAllocator;
use usbd_serial::{SerialPort, USB_CLASS_CDC};
use ufmt::uwriteln;
use rtt_target::{rprintln, rtt_init_print};

mod button_scanner;

define_scanner!(Scanner, 5, 4);

#[app(device = stm32f1xx_hal::pac, peripherals = true)]
const APP: () = {
    struct Resources {
        usb_serial: usbd_serial::SerialPort<'static, usb::UsbBusType>,
        usb_device: UsbDevice<'static, usb::UsbBusType>,
        timer: CountDownTimer<pac::TIM2>,
        scanner: Scanner<Pxx<Input<PullDown>>, Pxx<Output<PushPull>>>,
    }

    #[init]
    fn init(ctx: init::Context) -> init::LateResources {
        static mut USB_BUS: Option<UsbBusAllocator<usb::UsbBusType>> = None;

        // Alias peripherals
        let cp: cortex_m::Peripherals = ctx.core;
        let dp: pac::Peripherals = ctx.device;

        // Set up core registers
        let mut flash = dp.FLASH.constrain();
        let mut rcc = dp.RCC.constrain();
        
        // Init buffers for debug printing
        rtt_init_print!();

        let clocks = rcc.cfgr
            .use_hse(8.mhz())
            .sysclk(48.mhz())
            .pclk1(24.mhz())
            .adcclk(12.mhz())
            .freeze(&mut flash.acr);

        assert!(clocks.usbclk_valid());

        // Set up GPIO registers
        let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);
        let mut gpioc = dp.GPIOC.split(&mut rcc.apb2);

        let mut usb_disconnect = gpiob.pb0.into_push_pull_output(&mut gpiob.crl);
        
        // Set up the USB port
        let mut usb_dp = gpioa.pa12.into_push_pull_output(&mut gpioa.crh);
        usb_disconnect.set_low().unwrap();
        usb_dp.set_low().unwrap();
        asm::delay(clocks.sysclk().0 / 100);

        let usb = usb::Peripheral {
            usb: dp.USB,
            pin_dm: gpioa.pa11,
            pin_dp: usb_dp.into_floating_input(&mut gpioa.crh),
        };

        let (usb_serial, usb_device) = {
            *USB_BUS = Some(usb::UsbBus::new(usb));
            let serial = SerialPort::new(USB_BUS.as_ref().unwrap());

            let usb_dev = UsbDeviceBuilder::new(
                        USB_BUS.as_ref().unwrap(),
                        UsbVidPid(0x16c0, 0x27dd))
                .manufacturer("station.codes")
                .product("stm32gamepad")
                .serial_number("0x1")
                .device_class(USB_CLASS_CDC)
                .build();

            (serial, usb_dev)
        };

        rprintln!("stm32 gamepad");

        // Configure delay
        let _delay = Delay::new(cp.SYST, clocks);

        // Set up the timer
        let timer = Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1)
            .start_count_down(50.hz());

         let scanner = Scanner::new(
                [
                    gpiob.pb5.into_pull_down_input(&mut gpiob.crl).downgrade(),
                    gpiob.pb6.into_pull_down_input(&mut gpiob.crl).downgrade(),
                    gpiob.pb7.into_pull_down_input(&mut gpiob.crl).downgrade(),
                    gpiob.pb8.into_pull_down_input(&mut gpiob.crh).downgrade(),
                    gpiob.pb9.into_pull_down_input(&mut gpiob.crh).downgrade(),
                ],
                [
                    gpioc.pc0.into_push_pull_output(&mut gpioc.crl).downgrade(),
                    gpioc.pc1.into_push_pull_output(&mut gpioc.crl).downgrade(),
                    gpioc.pc2.into_push_pull_output(&mut gpioc.crl).downgrade(),
                    gpioc.pc3.into_push_pull_output(&mut gpioc.crl).downgrade(),
                ]
            );

        init::LateResources {
            usb_serial,
            usb_device,
            timer,
            scanner
        }
    }

    #[idle(resources = [usb_serial, timer, scanner])]
    fn idle(ctx: idle::Context) -> ! {
        let mut r = ctx.resources;

        rprintln!("idle loop start");
        loop {
            
            if let Ok(()) = r.timer.wait() {
                
                
                r.scanner.set_leds();
                
                let mut button = [0;5];
                let mut send_stuff = false;
                let mut index = 0;
                for input in &r.scanner.inputs {
                    if input.is_low().unwrap() {
                        button[index] = 1;
                        send_stuff = true;
                    } 
                    index += 1;
                }
                
                let magic = 9u8;
                if send_stuff {
                    r.usb_serial.lock(|serial| {
                        uwriteln!(
                            DirtyWriter(serial),
                            "{}{}{}{}{}{}",
                            magic,
                            button[0], // left
                            button[3], // right
                            button[4], // up
                            button[2], // down
                            button[1], // select
                        ).unwrap();
                    });
                }
            }
        }
    }

    #[task(binds = USB_LP_CAN_RX0, resources = [usb_device, usb_serial])]
    fn usb_lp_can_rx0(cx: usb_lp_can_rx0::Context) {
        usb_poll(cx.resources.usb_device, cx.resources.usb_serial);
    }

    #[task(binds = USB_HP_CAN_TX, resources = [usb_device, usb_serial])]
    fn usb_hp_can_tx0(cx: usb_hp_can_tx0::Context) {
        usb_poll(cx.resources.usb_device, cx.resources.usb_serial);
    }
};

fn usb_poll<B: usb_device::bus::UsbBus>(
    usb_dev: &mut UsbDevice<'static, B>,
    serial: &mut SerialPort<'static, B>,
) {
    if !usb_dev.poll(&mut [serial]) {
        return;
    }
    let mut buf = [0;10];
    match serial.read(&mut buf) {
        Ok(_) => {},
        Err(UsbError::WouldBlock) => {},
        e => panic!("USB read error: {:?}", e)
    }
}

pub struct DirtyWriter<'a, B: 'static + usb_device::bus::UsbBus>(
    &'a mut SerialPort<'static, B>
);

impl<'a, B: usb_device::bus::UsbBus> ufmt::uWrite for DirtyWriter<'a, B> {
    type Error = usb_device::UsbError;
    fn write_str(&mut self, s: &str) -> Result<(), Self::Error> {
        match self.0.write(&s.as_bytes()) {
            Ok(_) => Ok(()),
            Err(UsbError::WouldBlock) => Ok(()),
            Err(e) => Err(e)
        }
    }
}

