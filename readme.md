# getting started

## install cargo

<pre>rustup update</pre>
<pre>rustup target install thumbv7m-none-eabi</pre>
<pre>cargo install cargo-flash</pre>

## install probe-run

<pre>
cargo install probe-run
</pre>

## build

<pre>cargo build</pre>

## test

> todo

<pre>cargo test</pre>

## run

<pre>cargo run</pre>

## modules

### main

> todo: remove the usb hub, only one device

### button_scanner